import { Injectable } from '@angular/core';
import { Resource } from '../models/resource.model';
import { ContractService } from './contract.service';

@Injectable({
  providedIn: 'root'
})
export class ResourceService {

  constructor(private contractSrv: ContractService) { }

  private resources!: Resource[];

  setResources(resources: Resource[]) {
    this.resources = resources;
  }

  getResources() {
    if (!this.resources) {
      this.contractSrv.recupera().then(
        result => {
          if (result.length == 0) {
            console.log("recupera result empty");
          } else {
            let searchedResources: Resource[] = [];
            result.forEach((element: Resource) => {
              searchedResources.push(element);
              //Logging initial Resources in Contract
              //console.log("Resource found: { id: " + element.id + ", name: " + element.name + ", content: " + element.content + " }");
            });
            this.resources = searchedResources;
          }
        })
    }
    return this.resources;
  }

  addResource(resource: Resource) {
    this.resources.push(resource);
  }

}
