import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ContractService } from 'src/app/services/contract.service';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private contractSrv: ContractService, private router: Router) { }
  accountsFile: Map<string, string> = new Map();

  checkUndefinedAccount(){
    let undefinedAccount = this.contractSrv.getAccounts() == undefined ? true:false;
    if(undefinedAccount){
      this.router.navigate(['']);
    }
  }

  registerAccount(){
    let account = this.contractSrv.getAccounts();
    this.accountsFile.set(account[0], "Ganache Account 0");
    console.log(this.accountsFile);
    return this.accountsFile.get(account[0]);
  }

}
