import { Injectable } from '@angular/core';
import WalletConnectProvider from "@walletconnect/web3-provider";
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import Web3 from "web3";
import Web3Modal from "web3modal";
import { Resource } from '../models/resource.model';

@Injectable({
  providedIn: 'root'
})
export class ContractService {
  private web3js: any;
  private provider: any;
  private accounts: any;
  private testContract: any;
  private accountName: any;
  web3Modal

  private accountStatusSource = new Subject<any>();
  accStatusSubject = new Subject<any>();
  accountStatus$ = this.accountStatusSource.asObservable();

  constructor() {
    const providerOptions = {
      walletconnect: {
        package: WalletConnectProvider, // required
        options: {
          infuraId: "044895efc15547e1a27042c1d2874de0" // required
        }
      }
    };

    this.web3Modal = new Web3Modal({
      cacheProvider: true, // optional
      providerOptions, // required
      theme : "dark"
    //   theme: {
    //     background: "rgb(39, 49, 56)",
    //     main: "rgb(199, 199, 199)",
    //     secondary: "rgb(136, 136, 136)",
    //     border: "rgba(195, 195, 195, 0.14)",
    //     hover: "rgb(16, 26, 32)"
    //   }
    });

    this.addContractListener();
  }

  async connectAccount() {
    this.web3Modal.clearCachedProvider();
    this.provider = await this.web3Modal.connect(); // set provider
    this.web3js = new Web3(this.provider); // create web3 instance
    this.accounts = await this.web3js.eth.getAccounts(); 
    this.accountStatusSource.next(this.accounts);
    this.accStatusSubject.next(this.accounts);
  }

  //Singleton del contratto
  async getContract(){
    if(this.testContract != undefined){
      return this.testContract;
    }else{
      this.testContract = await new this.web3js.eth.Contract(environment.testcontract_abi, environment.testcontract_address);
      this.addContractListener();
      return this.testContract;
    }
  }
  
  getName(){
    return this.accountName;
  }

  getAccounts(){
    return this.accounts;
  }
 

  // -------------------------------------------------- RESOURCES ------------------------------------------------
  async crea(name: string, content: string){
    const create = await this.testContract.methods.newResource(name, content).send({ from: this.accounts[0]});
    return create;
  }

  async recuperaByName(name:string){
    const search = await this.testContract.methods.getResourceByName(name).call();
    return search;
  }

  async recuperaById(id:number){
    const search = await this.testContract.methods.searchById(id).call();
    return search;
  }

  async recupera(){
    await this.getContract();
    const search = await this.testContract.methods.getResources().call();
    return search;
  }

  async modifica(id: number, resource:Resource){
    const search = await this.testContract.methods.modifyResource(id, resource).send({ from: this.accounts[0] });
    return search;
  }

  async addContractListener(){
    this.testContract.events.createResource({}, (error: { message: string; }, data: { returnValues: any; }) => {
      if (error) {
        console.log("Error NewResource: " + error);
      } else {
        let id = data.returnValues.id;
        let name = data.returnValues.name;
        let content = data.returnValues.content;
        console.log("New Resource: { id: " + id + ", name: " + name + ", content: " + content + " }");
      }
    });
    console.log("Contract Listener agganciato");
  }

  // ------------------------------------------------------------------------------------------------------------
  // -------------------------------------------------- ACCOUNTS ------------------------------------------------
  // Aggiungere una nuova storage accounts nel contratto come mappa address --> String. 
  // String da cui recuperare il nome dell'account; Per ora gestiamo la cosa in account.service.ts
}