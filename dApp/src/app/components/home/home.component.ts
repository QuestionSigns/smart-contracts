import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ContractService } from 'src/app/services/contract.service';
import { ResourceService } from 'src/app/services/resource.service';
import { RestService } from 'src/app/services/rest.service';
import { AccountService } from '../../services/account.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {

  blockNumber?: string;
  blockNumberPresent: boolean = false;
  viewPage = false;


  nameSearch: string = "";
  idSearch!: number;

  constructor(private rs: RestService, private resourceSrv: ResourceService, private accountSrv: AccountService, private cs: ContractService, private route: Router) { }

  ngOnInit(): void {
    if(this.cs.getAccounts() == undefined){
      this.route.navigate(['']);
    }
    this.resourceSrv.getResources();
  }

  getBlockNumber() {
    this.rs.getBlockNumber().subscribe(
      value => {
        console.log(value.result);
        this.blockNumber = value.result;
        this.blockNumberPresent = true;
      },
      err => console.log(err),
    )
  }

}
