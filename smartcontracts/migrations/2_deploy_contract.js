const TestContract = artifacts.require("TestContract");
const UtilLib = artifacts.require("UtilLib");

module.exports = function (deployer) {
  deployer.deploy(UtilLib);
  deployer.link(UtilLib, TestContract);
  deployer.deploy(TestContract);
};