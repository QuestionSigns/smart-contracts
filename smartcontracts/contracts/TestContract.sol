//SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;
import "./UtilLib.sol";

contract TestContract {
    event createResource(uint id, string name, string content);
    // event retrieveResources(Resource[] resource);

    // struct User {
    //     address payable addr;
    //     Resource[] resources;
    // }

    struct Resource {
        uint id;
        string name;
        string content;
    }

    mapping(address => uint[]) public resources;
    Resource[] res;

    function newResource(string memory _name, string memory _content) public {
        //require(msg.value == 0.002 ether);
        uint id;
        
        if(res.length > 0){
            id = res.length;
        }else{
            id = 0;
        }

        res.push(Resource(id, _name, _content));
        resources[msg.sender].push(id);
        emit createResource(id, _name, _content);
    }


    function getResources() view public returns (Resource[] memory) {
        Resource[] memory getresource = res;
        return getresource;
    }

    function getResourceByName(string memory name) public view returns (Resource memory) {
        for(uint i = 0; i < res.length; i++){
            if(UtilLib.strcmp(name, res[i].name)){
                Resource memory resource = Resource(res[i].id, res[i].name, res[i].content);
                return (resource);
            }
        }
        return (Resource(404, "No Resource Found", ""));
    }

    function searchById(uint id) public view returns (Resource memory myResource) {
        for(uint i = 0; i < res.length; i++){
            if(id == res[i].id){
                Resource memory resource = Resource(res[i].id, res[i].name, res[i].content);
                return (resource);
            }
        }
        return (Resource(404, "No Resource Found", ""));
    }

    // function getResourceById(uint id) public view returns (Resource memory myResource) {
    //     Resource[] storage res = res;
    //     for(uint i = 0; i < res.length; i++){
    //         if(id == res[i].id){
    //             return (Resource(res[i].id, res[i].name, res[i].content));
    //         }
    //     }
    //     return (Resource(404, "No Resource Found", ""));
    // }

    // function modifyResource(uint id, string memory newContent) public {
    //     Resource memory myResource = searchById(id);
    //     myResource.content = newContent;
    //     emit createResource(myResource.name, myResource.content);
    // }

    function getBalance() public view returns(uint){
        return address(this).balance;
    } 

}
